# -*- coding: utf-8 -*-
import json
import simplejson
import time
import urllib
from query_to_topics import query_to_topics
import sys,getopt,datetime,codecs
if sys.version_info[0] < 3:
    import got
else:
    import got3 as got

def main(argv):

	if len(argv) == 0:
		print('You must pass some parameters. Use \"-h\" to help.')
		return

	if len(argv) == 1 and argv[0] == '-h':
		f = open('exporter_help_text.txt', 'r')
		print f.read()
		f.close()

		return
	query_topic = ''
	try:
		opts, args = getopt.getopt(argv, "", ("username=", "lang=", "near=", "within=", "since=", "until=", "querysearch=", "toptweets", "maxtweets=", "output="))

		tweetCriteria = got.manager.TweetCriteria()
		outputFileName = "output_got.json"

		for opt,arg in opts:
			if opt == '--username':
				tweetCriteria.username = arg

			elif opt == '--since':
				tweetCriteria.since = arg

			elif opt == '--until':
				tweetCriteria.until = arg

			elif opt == '--querysearch':
				tweetCriteria.querySearch = arg
				query_topic = tweetCriteria.querySearch.replace(" OR "," ")

			elif opt == '--toptweets':
				tweetCriteria.topTweets = True

			elif opt == '--maxtweets':
				tweetCriteria.maxTweets = int(arg)
			
			elif opt == '--near':
				city = arg
				tweetCriteria.near = '"' + arg + '"'
			
			elif opt == '--within':
				tweetCriteria.within = '"' + arg + '"'

			elif opt == '--within':
				tweetCriteria.within = '"' + arg + '"'

			elif opt == '--output':
				outputFileName = arg

			elif opt == '--lang':
				tweetCriteria.lang = arg
		import os
		os.chdir('tweets/')

		# outputFile.write('username;date;retweets;favorites;text;geo;mentions;hashtags;id;permalink')
		print('Searching...\n')

		for item in query_to_topics:
			if item['query'] == query_topic:
				topic = item['topic'].lower()

		def receiveBuffer(tweets):
			outputFile = open(outputFileName, "a+")
			outputFile.seek(0)
			try:
				l = json.load(outputFile)
				outputFile.seek(0)
				outputFile.truncate()
			except Exception, e:
				l = []
				print("error "+str(e))
			for t in tweets:
				d = {}
				# d['created_at'] = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(t.date, '%a %b %d %H:%M:%S +0000 %Y'))
				d['created_at'] = t.date.strftime("%a %b %d %H:%M:%S +0000 %Y")
				d['text'] = t.text
				d['user'] = {
					'name': t.username,
					'id': t.id,
					'screen_name': t.screenname
				}
				d['queryMetadata'] ={
					'query': tweetCriteria.querySearch,
					'query_topic': topic,
					'query_city': city,
					'query_city_range': tweetCriteria.within.replace("mi",""),
					'query_language': tweetCriteria.lang,
					'query_time': t.twitter_date + 172800000
				}
				d['coordinates'] = {
					'coordinates': t.geo
				}
				d['entities'] = {'user_mentions':[], 'hashtags':[], 'urls': [], 'media':[], 'symbols':[]}
				if t.data_reply_to_user_json is not None:
					for user_mention in t.data_reply_to_user_json:
						temp = {}
						temp['id'] = long(user_mention['id_str'])
						temp['screen_name'] = user_mention['screen_name']
						temp['name'] = user_mention['name']
						temp['id_str'] = str(user_mention['id_str'])
						d['entities']['user_mentions'].append(temp)
				if t.hashtags is not None:
					for hashtag in t.hashtags:
						temp = {}
						temp['text'] = hashtag
						d['entities']['hashtags'].append(temp)
				d['favorite_count'] = t.favorites
				d['retweet_count'] = t.retweets
				l.append(d)
				# outputFile.write(('\n%s;%s;%d;%d;"%s";%s;%s;%s;"%s";%s' % (t.username, t.date.strftime("%Y-%m-%d %H:%M"), t.retweets, t.favorites, t.text, t.geo, t.mentions, t.hashtags, t.id, t.permalink)))
			json.dump(l, outputFile, indent=2)
			outputFile.flush()
			outputFile.close()
			print('More %d saved on file...\n' % len(tweets))

		got.manager.TweetManager.getTweets(tweetCriteria, receiveBuffer)

	except arg:
		print('Arguments parser error, try -h' + arg)
	finally:
		print('Done. Output file generated "%s".' % outputFileName)

if __name__ == '__main__':
	main(sys.argv[1:])
