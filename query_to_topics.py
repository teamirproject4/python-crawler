# query_to_topics = [
#   {
#     "query": "air quality",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "floods",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "drought",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "dust storms",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "smog",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "pollution",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "air pollution",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "water pollution",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "land pollution",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "ozone",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "ozone depletion",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "earthquakes",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "greenhouse gases",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "fog",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "environment",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "hurricane",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "tornado",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "climate",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "industrial waste",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "resource depletion",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "climate change",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "extreme weather",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "oil spills",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "soil erosion",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "waste management",
#     "topic": "ENVIRONMENT"
#   },
#   {
#     "query": "crime",
#     "topic": "CRIME"
#   },
#   {
#     "query": "fraud",
#     "topic": "CRIME"
#   },
#   {
#     "query": "murder",
#     "topic": "CRIME"
#   },
#   {
#     "query": "espionage",
#     "topic": "CRIME"
#   },
#   {
#     "query": "tax evasion",
#     "topic": "CRIME"
#   },
#   {
#     "query": "embezzlement",
#     "topic": "CRIME"
#   },
#   {
#     "query": "rape",
#     "topic": "CRIME"
#   },
#   {
#     "query": "sexual assault",
#     "topic": "CRIME"
#   },
#   {
#     "query": "metoo",
#     "topic": "CRIME"
#   },
#   {
#     "query": "felony",
#     "topic": "CRIME"
#   },
#   {
#     "query": "perjury",
#     "topic": "CRIME"
#   },
#   {
#     "query": "smuggling",
#     "topic": "CRIME"
#   },
#   {
#     "query": "harrassment",
#     "topic": "CRIME"
#   },
#   {
#     "query": "crime rate",
#     "topic": "CRIME"
#   },
#   {
#     "query": "donald trump",
#     "topic": "POLITICS"
#   },
#   {
#     "query": "narendra modi",
#     "topic": "POLITICS"
#   },
#   {
#     "query": "edouard philippe",
#     "topic": "POLITICS"
#   },
#   {
#     "query": "prayut chan-o-cha",
#     "topic": "POLITICS"
#   },
#   {
#     "query": "enrique pena nieto",
#     "topic": "POLITICS"
#   },
#   {
#     "query": "vladimir putin",
#     "topic": "POLITICS"
#   },
#   {
#     "query": "donald trump",
#     "topic": "POLITICS"
#   },
#   {
#     "query": "Kim Jong-un",
#     "topic": "POLITICS"
#   },
#   {
#     "query": "Xi Jinping",
#     "topic": "POLITICS"
#   },
#   {
#     "query": "Theresa May",
#     "topic": "POLITICS"
#   },
#   {
#     "query": "foreign policy",
#     "topic": "POLITICS"
#   },
#   {
#     "query": "mayor",
#     "topic": "POLITICS"
#   },
#   {
#     "query": "prime minister",
#     "topic": "POLITICS"
#   },
#   {
#     "query": "political system",
#     "topic": "POLITICS"
#   },
#   {
#     "query": "governer of state",
#     "topic": "POLITICS"
#   },
#   {
#     "query": "strikes",
#     "topic": "social unrest"
#   },
#   {
#     "query": "protests",
#     "topic": "social unrest"
#   },
#   {
#     "query": "riots",
#     "topic": "social unrest"
#   },
#   {
#     "query": "rebellion",
#     "topic": "social unrest"
#   },
#   {
#     "query": "social unrest",
#     "topic": "social unrest"
#   },
#   {
#     "query": "state of emergency",
#     "topic": "social unrest"
#   },
#   {
#     "query": "insurgency",
#     "topic": "social unrest"
#   },
#   {
#     "query": "civil disobedience",
#     "topic": "social unrest"
#   },
#   {
#     "query": "boycott",
#     "topic": "social unrest"
#   },
#   {
#     "query": "civil war",
#     "topic": "social unrest"
#   },
#   {
#     "query": "sewage",
#     "topic": "INFRASTRUCTURE"
#   },
#   {
#     "query": "parks",
#     "topic": "INFRASTRUCTURE"
#   },
#   {
#     "query": "infrastructure",
#     "topic": "INFRASTRUCTURE"
#   },
#   {
#     "query": "roads",
#     "topic": "INFRASTRUCTURE"
#   },
#   {
#     "query": "canal",
#     "topic": "INFRASTRUCTURE"
#   },
#   {
#     "query": "airport",
#     "topic": "INFRASTRUCTURE"
#   },
#   {
#     "query": "bridge",
#     "topic": "INFRASTRUCTURE"
#   },
#   {
#     "query": "dams",
#     "topic": "INFRASTRUCTURE"
#   },
#   {
#     "query": "railways",
#     "topic": "INFRASTRUCTURE"
#   },
#   {
#     "query": "subway system",
#     "topic": "INFRASTRUCTURE"
#   },
#   {
#     "query": "harbors",
#     "topic": "INFRASTRUCTURE"
#   },
#   {
#     "query": "urban planning",
#     "topic": "INFRASTRUCTURE"
#   }
# ]
query_to_topics = [
  {
    "query": "donald trump",
    "topic": "POLITICS"
  }
]