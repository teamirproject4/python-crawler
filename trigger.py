import subprocess
from datetime import timedelta, date
from query_to_topics import query_to_topics
import urllib2
import urllib

cities = ['nyc', 'delhi', 'bangkok', 'paris', 'mexico city']

lang = ['en', 'es', 'fr', 'th', 'hi']

radius = ['2', '100']

def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)

start_date = date(2018, 8, 27)
end_date = date(2018, 11, 29)

for single_date in daterange(start_date, end_date):
    since = single_date.strftime("%Y-%m-%d")
    until = single_date + timedelta(1)
    until = until.strftime("%Y-%m-%d")
    print "since" + since
    print "until" + until
    for query in query_to_topics:
        for city in cities:
            for l in lang:
                for r in radius:
                    querysearch = '"' + query['query'] + '"'
                    querysearch = querysearch.replace(" ", " OR ")
                    # querysearch = urllib.quote_plus(query['query'])
                    print "querysearch" + querysearch
                    topic = query['topic'].lower()
                    output_file = "{}_{}_{}_{}.json".format(querysearch, city, l, r)
                    print "output_file   " + output_file
                    command = "python Exporter.py --querysearch {} --since {} --until {} --near {} --lang {} --output {}".format(querysearch, since, until, city, l, output_file)
                    import os
                    print "command  " + command
                    try:
                      os.system(command)
                    except Exception:
                      print Exception